# Correction
## Task 1 
###Task 1.1

8/8 P
###Task 1.2
Some transposing is missing.
1/2 P

## Task 2 
missing 
0/10 P

#Total : 9 / 20 P



**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 11 RBMs**

Date: 13.07.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |



**Table of Contents**

- [task1: Gibbs Sampling [10 P]](#task1-gibbs-sampling-10-p)
    - [task1.1. Implement the Gibbs sampling steps [8 P]](#task11-implement-the-gibbs-sampling-steps-8-p)
        - [Method `sample_prob`](#method-sampleprob)
        - [Methods `sample_h_given_v` and `sample_v_given_h`](#methods-samplehgivenv-and-samplevgivenh)
        - [Methods `gibbs_hvh` and `gibbs_vhv`](#methods-gibbshvh-and-gibbsvhv)
    - [task1.2. Implement the weight updates. [2 P]](#task12-implement-the-weight-updates-2-p)
- [task2: MNIST [10 P]](#task2-mnist-10-p)
    - [task2.1. Use CD1 and CD10 for at least 50 epochs with a suitable learning rate. [4 P]](#task21-use-cd1-and-cd10-for-at-least-50-epochs-with-a-suitable-learning-rate-4-p)
    - [task2.2. Plot the weight matrices and the validation loss. [2 P]](#task22-plot-the-weight-matrices-and-the-validation-loss-2-p)
    - [task2.3. Sample from the learned model. [4 P]](#task23-sample-from-the-learned-model-4-p)
- [References](#references)



# task1: Gibbs Sampling [10 P] #

&gt; The file `rbm.py` contains a model of a restricted Boltzmann machine. Complete the code.



## task1.1. Implement the Gibbs sampling steps [8 P] ##

The corresponding experiment with test output is experiment [5db811681](https://www.comet.ml/irratzo/exercise11/5db811681bc4483da31d5245988e6006).

The `RBM` class methods `sample_prob`, `sample_h_given_v`, `sample_v_given_h`,
`gibbs_hvh`, `gibbs_vhv`, correspond to step 2. and 3., *positive phase* resp. *negative
phase*, in the CD1 algorithm given in the lecture slides [1], p. 18.

### Method `sample_prob` ###

The code hint is: `sample binary units from real-valued probabilities: v_i/h_j =
1 if p_{i/j} &gt; rand()`.

We implemented this with using `tf.compare`, which returns booleans and `tf.cast` to `tf.float32`:

```python
    # sample binary units from real-valued probabilities: v_i/h_j = 1 if p_{i/j} &gt; rand()
    def sample_prob(self, probs):
        # input: probabilities &quot;probs&quot; of visible or hidden units
        # output: matrix with binary units of same size as probs (v_i/h_j = 1 if probs &gt; rand())
        # pass # TODO
        return tf.cast(tf.math.greater(probs,tf.random.uniform(probs.shape)), tf.float32)
```

For the 32x1 test tensor, this mapping yields:

```
from 

tf.Tensor(
[[-0.18153107  0.08040011 -0.84452057 -0.04651022 -0.00874078  0.6449543
  -0.04996479  0.24382973 -0.65587556  0.11518228  0.27506602  0.29288816
   0.2730068  -0.16367126  0.38047588  0.07290876 -0.90116525 -0.96896183
   0.3491001  -0.17329538 -0.18380189  0.23605561 -0.32569146 -0.64084625
   0.31210363 -0.22943461 -0.29113913  0.28617144  0.2717955  -0.35787892
  -0.2293719  -0.08319449]], shape=(1, 32), dtype=float32)
  
to

tf.Tensor(
[[0. 0. 0. 1. 0. 1. 1. 0. 0. 0. 0. 0. 1. 0. 1. 0. 0. 0. 1. 0. 1. 1. 0. 0.
  0. 0. 0. 1. 0. 0. 0. 0.]], shape=(1, 32), dtype=float32)
```

### Methods `sample_h_given_v` and `sample_v_given_h` ###

See also [1] p.14.

``` python
    # calculate hidden units from visible units
    def sample_h_given_v(self, vis):
        # input: visible units
        # output: h_mean (probabilities of hidden units)
        #         h_sample (hidden binary units)
        # pass # TODO
        # prob. of hj=1 given the visible units x
        h1_given_v = tf.nn.sigmoid(tf.matmul(vis, self.W) + self.hbias)
        h_sample = self.sample_prob(h1_given_v)
        return [h1_given_v, h_sample]
        
    # reconstruct visible units from hidden units
    def sample_v_given_h(self, hid):
        # input: hidden units
        # output: v_mean (probabilities of visible units)
        #         v_sample (visible binary units)
        # pass # TODO
        # prob. of visible unit xi=1 given the hidden units h
        v1_given_h = tf.nn.sigmoid(tf.matmul(hid, tf.transpose(self.W)) + self.vbias)
        v_sample = self.sample_prob(v1_given_h)
        return [v1_given_h, v_sample]
```

### Methods `gibbs_hvh` and `gibbs_vhv` ###

These Gibbs sampling steps give us the averages `_data` and
`_model` for repeated sampling and thus updating our model parameters
(weights) by the Contrast Divergence (CD) method, as explained in [1] p.16,
p.17.

``` python
    # Gibbs sampling step: begin with hidden units
    def gibbs_hvh(self, h0_sample):
        # calculate visible units v1_mean, v1_sample from hidden units h0_sample
        # calculate hidden units h1_mean, h1_sample from visible units v1_sample
        # pass # TODO
        v1_given_h0, v1_sample = self.sample_v_given_h(h0_sample)
        h1_given_v1, h1_sample = self.sample_h_given_v(v1_sample)
        return [v1_given_h0, v1_sample,
                h1_given_v1, h1_sample]   
    
    # Gibbs sampling step: begin with visible units
    def gibbs_vhv(self, v0_sample):
        # calculate hidden units h0_mean, h0_sample from visible units v0_sample
        # calculate from units v1_mean, v1_sample from hidden units h0_sample
        # pass # TODO
        h1_given_v0, h1_sample = self.sample_h_given_v(v0_sample)
        v1_given_h1, v1_sample = self.sample_v_given_h(h1_sample)
        return [h1_given_v0, h1_sample,
                v1_given_h1, v1_sample]
```

## task1.2. Implement the weight updates. [2 P] ##

# task2: MNIST [10 P] #

&gt; Train the given RBM model in file exercise11.py on the MNIST dataset.

## task2.1. Use CD1 and CD10 for at least 50 epochs with a suitable learning rate. [4 P] ##

## task2.2. Plot the weight matrices and the validation loss. [2 P] ##

## task2.3. Sample from the learned model. [4 P] ##

&gt; Start the Markov chain with 10 test samples showing digits from 0 to 9. Draw images for every 100 steps of Gibbs sampling. 

# References #

- [1] RWTH DLiPR SS20 Part 11: Restricted Boltzmann Machines. Lecture Slides. 2020.
- [2] deeplearning.net. Restricted Boltzmann Machines (RBM). Retrieved 13.07.2020. URL: http://deeplearning.net/tutorial/rbm.html



