from comet_ml import Experiment
import numpy as np
import matplotlib.pyplot as plt
from rbm import RBM
import dlipr

# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise11", workspace="EnterGroupWorkspaceHere")


data = dlipr.mnist.load_data()

X_train = data.train_images.reshape(-1, 28**2).astype('float32') / 255
X_test = data.test_images.reshape(-1, 28**2).astype('float32') / 255
# convert input to stochastic binary states
X_train[X_train > 0.5] = 1.
X_train[X_train <= 0.5] = 0.
X_test[X_test > 0.5] = 1.
X_test[X_test <= 0.5] = 0.

# rbm model
rbm = RBM(X_train=X_train, X_test=X_test, n_visible=28*28, n_hidden=100, W=None, hbias=None, vbias=None)

"""
TASK 2:

1. Train the given RBM with CD-1 and CD-10 for at least 50 epochs with a suitable learning rate.

2. Plot the weight matrices and the validation loss.

3. Sample from learned model, start Markov chain with 10 test samples showing digits from 0-9.
   Draw images for every 100 steps of Gibbs sampling. Don't forget to track the plots using COMET!
"""
