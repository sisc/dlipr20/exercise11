"""
TO DO:

1. Implement Gibbs sampling by completing the functions
   - sample_prob
   - sample_h_given_v
   - sample_v_given_h
   - gibbs_hvh
   - gibbs_vhv

2. Perform the weight updates in the function "get_cost_updates"

By executing this file you can test if your function produce sensible output!

---------------------------------------------------------------
--------------------------------------------------------------

Useful commands:

math-operations:
tf.matmul
tf.random_uniform
tf.shape
tf.sign

activation functions:
tf.nn.relu, tf.nn.sigmoid

udating variables:
a.assign_add(b) -> value of variable a is updated by adding output value of b

"""
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import os
import dlipr


class RBM(object):
    """ Restricted Boltzmann Machine (RBM) """

    def __init__(self, X_train=None, X_test=None, n_visible=28*28, n_hidden=100, W=None, hbias=None, vbias=None):

        self.X_train = X_train
        self.X_test = X_test
        self.n_visible = n_visible
        self.n_hidden = n_hidden

        # initialize weights and biases

        # weight matrix
        if W is None:
            W = tf.Variable(tf.random_uniform([n_visible, n_hidden],
                                              minval=-4 * tf.sqrt(6. / (n_hidden + n_visible)),
                                              maxval=4 * tf.sqrt(6. / (n_hidden + n_visible))))
        # bias vector of hidden units
        if hbias is None:
            hbias = tf.Variable(tf.zeros([n_hidden]))
        # bias vector of visible units
        if vbias is None:
            vbias = tf.Variable(tf.zeros([n_visible]))

        self.X = None
        self.W = W
        self.hbias = hbias
        self.vbias = vbias
        self.w_upd8 = None
        self.hbias_upd8 = None
        self.vbias_upd8 = None
        self.cost = None

    # sample binary units from real-valued probabilities: v_i/h_j = 1 if p_{i/j} > rand()
    def sample_prob(self, probs):
        # input: probabilities "probs" of visible or hidden units
        # output: matrix with binary units of same size as probs (v_i/h_j = 1 if probs > rand())

        # calculate hidden units from visible units

    def sample_h_given_v(self, vis):
        # input: visible units
        # output: h_mean (probabilities of hidden units)
        #         h_sample (hidden binary units)

        # reconstruct visible units from hidden units

    def sample_v_given_h(self, hid):
        # input: hidden units
        # output: v_mean (probabilities of visible units)
        #         v_sample (visible binary units)

        # Gibbs sampling step: begin with hidden units

    def gibbs_hvh(self, h0_sample):
        # calculate visible units v1_mean, v1_sample from hidden units h0_sample
        # calculate hidden units h1_mean, h1_sample from visible units v1_sample

        # Gibbs sampling step: begin with visible units

    def gibbs_vhv(self, v0_sample):
        # calculate hidden units h0_mean, h0_sample from visible units v0_sample
        # calculate from units v1_mean, v1_sample from hidden units h0_sample

        # -----------------------------------------------------------
        #   set up model
        # -----------------------------------------------------------

    def get_cost_updates(self, lr=0.1, k=1):

        # positive phase: calculate hidden units from input data
        ph_means, ph_samples = self.sample_h_given_v(self.X)

        # Constrastive divergence: CD_k
        # negative phase: alternating calculation of visible (real-valued) and
        #                 hidden units (stochastic binary states)
        for step in range(k):
            if step == 0:
                nv_means, nv_samples, nh_means, nh_samples = self.gibbs_hvh(ph_samples)
            else:
                nv_means, nv_samples, nh_means, nh_samples = self.gibbs_hvh(nh_samples)

        # calculate variable updates
        """
        Take a similar approach for updating the weights as it is shown here for the biases
        - For x_0 and h_0 use self.X and ph_samples
        - For x_k and h_k use nv_samples and nh_means
          (take probabilities of final hidden units nh_means to avoid unnecessary sampling noise)
        """

        self.vbias_upd8 = self.vbias.assign_add(lr * tf.reduce_mean(self.X - nv_samples, 0))
        self.hbias_upd8 = self.hbias.assign_add(lr * tf.reduce_mean(ph_samples - nh_means, 0))

        # objective function: binary crossentropy
        monitoring_cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.X, logits=nv_samples))

        self.cost = monitoring_cost

        return monitoring_cost


# -----------------------------------------------------------
#       training loop
# -----------------------------------------------------------

    def training(self, lr=0.1, k=1, n_epoch=3, batch_size=32):

        cost = []

        for epoch in range(n_epoch):
            # number of updates per epoch
            n_updates = len(self.X_train)//batch_size
            # shuffle training set
            perm = np.random.choice(len(self.X_train), size=len(self.X_train), replace='False')

            for i in range(n_updates):
                # training batch
                batch = self.X_train[perm[i*batch_size:(i+1)*batch_size]]

                # train on batch: update weights, biases and training loss
                self.X = batch
                self.get_cost_updates(lr=lr, k=k, updates=True)

            # update validation loss
            cost.append(self.cost)

        # save final model, get weights and biases
        W = self.W
        hbias = self.hbias
        vbias = self.vbias

        return W, hbias, vbias, cost

# -----------------------------------------------------------
#   Set up generating model
# -----------------------------------------------------------

    def daydream(self, k=50):

        for step in range(k):

            if step == 0:
                nh_means, nh_samples, nv_means, nv_samples = self.gibbs_vhv(self.X)
            else:
                # for better stability of reconstruction use probabilities for visible units
                # during image generation phase
                nh_means, nh_samples, nv_means, nv_samples = self.gibbs_vhv(nv_means)

        return nv_means, nv_samples

# -----------------------------------------------------------
#   Generate new samples from input data
# -----------------------------------------------------------

    def images(self, inputdata=None, k=50):

        try:
            self.X = inputdata
        except:
            print('No inputdata')
        vis_mean, vis_samples = self.daydream(k=k)

        return vis_mean, vis_samples


# -----------------------------------------------------------
#   Test of Task 1
# -----------------------------------------------------------
if __name__ == '__main__':

    """
    Task 1.1: Implement Gibbs sampling
    """

    data = dlipr.mnist.load_data()
    X_test = data.test_images.reshape(-1, 28**2).astype('float32')/255
    X_test_image = X_test[0:2]
    X_test[X_test > 0.5] = 1.
    X_test[X_test <= 0.5] = 0.

    rbm = RBM(X_train=X_test_image, X_test=X_test_image)

    """
    1. Test sample_prob
    """
    probs = tf.random.uniform([1, 32], minval=0., maxval=1.)
    sample_binary = rbm.sample_prob(probs)

    print('================= \n 1. sample_prob \n=================')
    print('Probabily vector: \n' + str(probs.numpy()) + '\n')
    print('Binary units: \n' + str(sample_binary.numpy()))

    """
    2./3. Test sample_h_given_v and sample_v_given_h with first two images of the test set
    """
    h_mean, h_sample = rbm.sample_h_given_v(X_test_image)


    print('================= \n 2. sample_h_given_v \n=================')
    # only hidden units of first image are printed
    print('Probabily h_mean: \n' + str(h_mean[0].numpy()) + '\n')
    print('Binary units h_sample: \n' + str(h_sample[0].numpy()) + '\n')
    print('shape(h_sample)/shape(h_mean): ' + str(tf.shape(h_sample).numpy()) + '\n\n')

    v_mean, v_sample = rbm.sample_v_given_h(h_sample)

    print('================= \n 3. sample_h_given_v \n=================')
    print('Probabily v_mean: \n' + str(v_mean.numpy()) + '\n')
    print('Binary units v_sample: \n' + str(v_sample.numpy()) + '\n')
    print('shape(v_sample)/shape(v_mean): ' + str(tf.shape(v_sample).numpy()) + '\n\n')

    """
    4./5. Test gibbs_vhv and gibbs_hvh with first image of the test set
    """
    h0_mean, h0_sample, v1_mean, v1_sample = rbm.gibbs_vhv(X_test_image)

    print('================= \n 4. gibbs_vhv \n=================')
    print('shape(h0_sample)/shape(h0_mean): ' + str(tf.shape(h0_sample).numpy()) + '\n')
    print('shape(v1_sample)/shape(v1_mean): ' + str(tf.shape(v1_sample).numpy()) + '\n\n')

    v1_mean, v1_sample, h1_mean, h1_sample = rbm.gibbs_hvh(h0_sample)

    print('================= \n 5. gibbs_hvh \n=================')
    print('shape(v1_sample)/shape(v1_mean): ' + str(tf.shape(v1_sample).numpy()) + '\n')
    print('shape(h1_sample)/shape(h1_mean): ' + str(tf.shape(h1_sample).numpy()) + '\n\n')



    """
    Task 1.2 : Verify weight updates
    """

    for i in range(2):
        W, hbias, vbias, cost = rbm.training(lr=0.001, k=1, n_epoch=1, batch_size=1)
        print(W.numpy())
